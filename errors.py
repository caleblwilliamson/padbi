import discord
from discord.ext import commands

class errorCodes:
    argMiss = "required argument that is missing"
    argMissMsg = "You are missing an argument for this command. Try `!help`"
    invalidInt = "ValueError: invalid literal for int()"
    invalidIntMsg = "Pass an integer only - e.g. `!purge 10`"
    noPerms = "The check functions for command"
    noPermsMsg = "Sorry, not enough permissions."
