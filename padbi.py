import discord, logging, os, datetime, pyodbc
from discord.ext import commands

# MSSQL connection
pw = os.environ['PADBI_SQL']
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER=192.168.50.130;DATABASE=Discord;UID=padbi;PWD='+pw)
cursor = cnxn.cursor()

# Initialize bot
bot = commands.Bot(command_prefix = '!')

# Configure logging for the bot
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

# Non API error handling
async def customError(x, x2):
    errorStr = str(x)
    cursor.execute("SELECT TOP (100) * FROM ErrorCodes WHERE Error  like ?", "%{}%".format(errorStr))
    rows = cursor.fetchall()

    for i in rows:
        if errorStr in i.Error:
            await x2.send(i.ErrorMessage)
        else:
            await x2.send(errorStr)

# SQL insert function
def insertArchive(x):
    cursor.execute("INSERT INTO Archive (Author, Message, DateSent) values(?,?, CURRENT_TIMESTAMP)",
               (str(x.author), str(x.content)))
    cnxn.commit()

# BEGIN events & commands
@bot.event
async def on_message(message):
    # Define channel, guild & message content vars for future use
    channel = message.channel 
    guild = message.guild
    msgCont = message.content

    # Archive all messages sent
    insertArchive(message)

    # Ignore messages from bot user
    if message.author == bot.user:
        return

    # Warn against use of TTS
    if message.tts:
        await channel.send('Subsequent uses of the `/tts` command will result in a permanent ban.')
        await message.add_reaction('👎')
        # Pull member OBJECT from message author id 
        print(guild.get_member(message.author.id))

    # React to messages containing 'hostek'
    if "hostek" in msgCont.lower():
        await message.add_reaction('😂')

    # Process all bot commands - DO NOT MOVE
    await bot.process_commands(message)

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))
    await bot.change_presence(activity=discord.Game(name='!help'))

@bot.event
async def on_command_error(ctx, error):
    errorStr = str(error)
    cursor.execute("SELECT TOP (100) * FROM ErrorCodes WHERE Error like ?", "%{}%".format(errorStr))
    rows = cursor.fetchall()

    for i in rows:
        if errorStr in i.Error:
            try:
                await ctx.send(i.ErrorMessage)
                print(i.ErrorMessage)
            except:
                await ctx.send(errorStr)

@bot.command()
async def echo(ctx, arg):
    " ~~ Returns the value that is passed to it"
    await ctx.send(arg)

@bot.command()
@commands.has_role('admin')
async def purge(ctx, amount, error):
    "~~ Purges messages from a channel - try `!purge 10` to delete 10 messages"
    try:
        await ctx.channel.purge(limit=int(amount))
    except:
        errorStr = str(error)
        print(errorStr)

@bot.command()
async def guildchk(ctx):
    membersObj = bot.get_all_members()
    for member in membersObj:
        print(member.name, member.id)

@bot.command()
async def goodbot(ctx):
    await ctx.send("woof!")

@bot.command()
@commands.has_role('admin')
async def kick(ctx, user):
    "~~ Kicks specified user from server - try `!kick @user_here`"
    guild = ctx.message.guild
    target = ctx.message.mentions[0]
    try:
        await guild.kick(target)
        await ctx.send("User has been kicked!")
    except:
        await ctx.send("Something went wrong :(")
    

# END events &s commands


bot.run(os.environ['DISCORD_TOKEN'])
