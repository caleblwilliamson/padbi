#! /bin/bash
# This file is only present within the repo in the interest of being inclusive. The scripts should be present on the runner, within the /scripts/ directory, as that is where it is called and utilized. 

# This short script removes all active containers from a specified text file

txtfile=containers.txt
containers=`cat $txtfile`

for line in $containers ;
do
    docker rm -f $line
done
echo $containers